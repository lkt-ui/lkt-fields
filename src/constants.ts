import {Settings} from "./settings/Settings";

export const FIELD_STATE_PROPS = {
    showAdd: {type: Boolean, default: false},
    showLink: {type: Boolean, default: false},
    showOpen: {type: Boolean, default: false},
    showInfo: {type: Boolean, default: false},
    showPassword: {type: Boolean, default: false},
    showMandatory: {type: Boolean, default: false},
    showError: {type: Boolean, default: false},
    showWarn: {type: Boolean, default: false},
    showLog: {type: Boolean, default: false},
    showReset: {type: Boolean, default: false},
    textAdd: {type: String, default: ''},
    textInfo: {type: String, default: 'More info'},
    textPassword: {type: String, default: Settings.SHOW_PASSWORD_MESSAGE},
    textMandatory: {type: String, default: Settings.IS_MANDATORY_MESSAGE},
    textError: {type: String, default: ''},
    textWarn: {type: String, default: ''},
    textLog: {type: String, default: ''},
    textLink: {type: String, default: Settings.FOLLOW_LINK_MESSAGE},
    textOpen: {type: String, default: Settings.OPEN_MESSAGE},
    textReset: {type: String, default: Settings.RESET_MESSAGE},
};

export const SHOW_PASSWORD_MESSAGE = 'Show password';
export const IS_MANDATORY_MESSAGE = 'This is mandatory';
export const FOLLOW_LINK_MESSAGE = 'Follow link';
export const OPEN_MESSAGE = 'Show details';
export const RESET_MESSAGE = 'Reset';

export const NO_OPTIONS_MESSAGE = 'No results. Try typing some text.';

export const TODAY_RANGE_TEXT = 'Today';
export const THIS_MONTH_RANGE_TEXT = 'This month';
export const LAST_MONTH_RANGE_TEXT = 'Last month';
export const THIS_YEAR_RANGE_TEXT = 'This year';

export const DEFAULT_PLUGIN_OPTIONS: IPluginOptions = {
    noOptionsMessage: NO_OPTIONS_MESSAGE,
    showPasswordMessage: SHOW_PASSWORD_MESSAGE,
    isMandatoryMessage: IS_MANDATORY_MESSAGE,
    followLinkMessage: FOLLOW_LINK_MESSAGE,
    openMessage: OPEN_MESSAGE,
    resetMessage: RESET_MESSAGE,
    todayRangeText: TODAY_RANGE_TEXT,
    thisMonthRangeText: THIS_MONTH_RANGE_TEXT,
    lastMonthRangeText: LAST_MONTH_RANGE_TEXT,
    thisYearRangeText: THIS_YEAR_RANGE_TEXT,
}

export const DEFAULT_EDITOR_BUTTONS = [
    ['undo', 'redo'],
    ['font', 'fontSize', 'formatBlock'],
    ['paragraphStyle', 'blockquote'],
    ['bold', 'underline', 'italic', 'strike', 'subscript', 'superscript'],
    ['fontColor', 'hiliteColor', 'textStyle'],
    ['removeFormat'],
    '/', // Line break
    ['outdent', 'indent'],
    ['align', 'horizontalRule', 'list', 'lineHeight'],
    ['table', 'link', /**'image', 'video', 'audio',*/],
    /** ['imageGallery'] */ // You must add the "imageGalleryUrl".
    ['fullScreen', 'showBlocks', 'codeView'],
    ['preview', 'print'],
    [/**'save',*/ 'template']
];

export const DEFAULT_KATEX_BUTTONS = [
    ['undo', 'redo'],
    ['font', 'fontSize', 'formatBlock'],
    ['paragraphStyle', 'blockquote'],
    ['bold', 'underline', 'italic', 'strike', 'subscript', 'superscript'],
    ['fontColor', 'hiliteColor', 'textStyle'],
    ['removeFormat'],
    '/', // Line break
    ['outdent', 'indent'],
    ['align', 'horizontalRule', 'list', 'lineHeight'],
    ['table', 'link', /**'image', 'video', 'audio',*/ 'math'],
    /** ['imageGallery'] */ // You must add the "imageGalleryUrl".
    ['fullScreen', 'showBlocks', 'codeView'],
    ['preview', 'print'],
    [/**'save',*/ 'template']
];