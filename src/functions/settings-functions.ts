import {Settings} from "../settings/Settings";

export const getNoOptionsMessage = () => {
    return Settings.NO_OPTIONS_MESSAGE;
}