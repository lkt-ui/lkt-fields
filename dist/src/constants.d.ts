export declare const FIELD_STATE_PROPS: {
    showAdd: {
        type: BooleanConstructor;
        default: boolean;
    };
    showLink: {
        type: BooleanConstructor;
        default: boolean;
    };
    showOpen: {
        type: BooleanConstructor;
        default: boolean;
    };
    showInfo: {
        type: BooleanConstructor;
        default: boolean;
    };
    showPassword: {
        type: BooleanConstructor;
        default: boolean;
    };
    showMandatory: {
        type: BooleanConstructor;
        default: boolean;
    };
    showError: {
        type: BooleanConstructor;
        default: boolean;
    };
    showWarn: {
        type: BooleanConstructor;
        default: boolean;
    };
    showLog: {
        type: BooleanConstructor;
        default: boolean;
    };
    showReset: {
        type: BooleanConstructor;
        default: boolean;
    };
    textAdd: {
        type: StringConstructor;
        default: string;
    };
    textInfo: {
        type: StringConstructor;
        default: string;
    };
    textPassword: {
        type: StringConstructor;
        default: string;
    };
    textMandatory: {
        type: StringConstructor;
        default: string;
    };
    textError: {
        type: StringConstructor;
        default: string;
    };
    textWarn: {
        type: StringConstructor;
        default: string;
    };
    textLog: {
        type: StringConstructor;
        default: string;
    };
    textLink: {
        type: StringConstructor;
        default: string;
    };
    textOpen: {
        type: StringConstructor;
        default: string;
    };
    textReset: {
        type: StringConstructor;
        default: string;
    };
};
export declare const SHOW_PASSWORD_MESSAGE = "Show password";
export declare const IS_MANDATORY_MESSAGE = "This is mandatory";
export declare const FOLLOW_LINK_MESSAGE = "Follow link";
export declare const OPEN_MESSAGE = "Show details";
export declare const RESET_MESSAGE = "Reset";
export declare const NO_OPTIONS_MESSAGE = "No results. Try typing some text.";
export declare const TODAY_RANGE_TEXT = "Today";
export declare const THIS_MONTH_RANGE_TEXT = "This month";
export declare const LAST_MONTH_RANGE_TEXT = "Last month";
export declare const THIS_YEAR_RANGE_TEXT = "This year";
export declare const DEFAULT_PLUGIN_OPTIONS: IPluginOptions;
export declare const DEFAULT_EDITOR_BUTTONS: (string | string[])[];
export declare const DEFAULT_KATEX_BUTTONS: (string | string[])[];
