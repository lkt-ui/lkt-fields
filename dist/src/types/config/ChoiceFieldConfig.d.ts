export type ChoiceFieldConfig = {
    type: 'select' | 'radio';
    multiple?: boolean;
};
