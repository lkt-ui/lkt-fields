export type SimpleFieldConfig = {
    type: 'text' | 'text-area' | 'email' | 'check' | 'switch';
};
